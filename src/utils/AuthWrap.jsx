import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const AuthWrap = ({ auth, exact, path, component, token }) => {
  if (token && !auth) return null;
  return auth ? <Route exact={exact} path={path} component={component} /> : <Redirect to={'/'} />;
};

export default AuthWrap;
