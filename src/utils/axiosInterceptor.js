import axios from 'axios';
import { signOut } from '../components/Auth/auth.action';
import setAuthToken from './setAuthToken';
import notification from './notification';

export default {
  init: store => {
    axios.interceptors.response.use(
      response => {
        if (response.headers.authorization) {
          localStorage.setItem('LS-FRONTEND', response.headers.authorization);
          setAuthToken(response.headers.authorization);
        }
        return response;
      },
      e => {
        if (e.response.status === 401) {
          store.dispatch(signOut());

          notification('error', 'Unathorised');
          return false;
        }
        return Promise.reject(e);
      }
    );
  },
};
