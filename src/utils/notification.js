import Alert from 'react-s-alert';

const notification = (type, text) => {
  return Alert[type](text, {
    position: 'top-right',
    effect: 'slide',
    timeout: 5000
  });
}

export default notification;