import { combineReducers } from "redux";
import { authReducer } from '../components/Auth/auth.reducer';
import { tableReducer } from '../components/Main/Table/table.reducer'

const reducers = combineReducers({
  auth: authReducer,
  table: tableReducer
});

export default reducers;
