export default {
  signIn: '/sign-in',
  signUp: '/sign-up',
  homepage: '/',
  data: '/data',
  mainTable: '/main-table',
  smthElse: '/smth-else',
  personalInfo: '/personal-info'
};
