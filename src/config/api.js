import axios from "axios";

export default {
  signIn: data => axios.post("/api/auth/sign-in", data),
  signUp: data => axios.post("/api/auth/sign-up", data),
  getUser: () => axios.get('/api/auth/user'),
  getTableData: () => axios.get('/api/table'),
  insertNewData: data => axios.post('/api/table', data),
  deleteData: id => axios.delete('/api/table', { data: { id } })
};
