import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import interceptors from './utils/axiosInterceptor'

import store from './store/configureStore';
// import * as serviceWorker from './serviceWorker';

interceptors.init(store);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,

  document.getElementById('root')
);

// serviceWorker.unregister();
