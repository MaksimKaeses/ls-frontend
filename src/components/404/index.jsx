import React from 'react';
import { Link } from 'react-router-dom';

const NothingFound = () => {
  return (
    <div className="wrapper flex-column">
      <span>404 error</span>
      <p>Page has not been found</p>
      <Link to="/">Go back to homepage</Link>
    </div>
  );
};

export default NothingFound;
