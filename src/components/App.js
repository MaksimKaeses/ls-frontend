import React, { Fragment, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux'
import routes from '../config/routes';
import Alert from 'react-s-alert'

import AuthWrap from '../utils/AuthWrap'
import SignIn from '../components/Auth/SignIn';
import SignUp from '../components/Auth/SignUp';
import Main from './Main';
import Homepage from './Homepage'
import Header from './Header'
import NothingFound from './404'

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

import '../css/index.css'
import setAuthToken from '../utils/setAuthToken';
import { getUserData } from '../components/Auth/auth.action'

const App = () => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading)
  const auth = useSelector(state => state.auth.user.apiKey);
  const token = window.localStorage.getItem('LS-FRONTEND');

  useEffect(() => {
    if (!auth && token) {
      setAuthToken(token);
      dispatch(getUserData());
    }
  }, [auth, dispatch, token])

  return (
    <Fragment>
      {loading ? (
        <div className="wrapper flex loading">
          loading...
        </div>
      ) : (
          <Fragment>
            <Header auth={auth} />
            <Switch>
              <Route path={routes.homepage} exact component={Homepage} />
              <Route path={routes.signIn} component={SignIn} />
              <Route path={routes.signUp} component={SignUp} />
              <AuthWrap auth={auth} token={token} path={routes.data} component={Main} />
              <Route component={NothingFound} />
            </Switch>
          </Fragment>
        )}
      <Alert stack={{ limit: 3 }} />
    </Fragment>
  );
};

export default App;
