import React from 'react';
import { NavLink } from 'react-router-dom';
import routes from '../../../config/routes';

const Aside = ({ match }) => {
  return (
    <aside className="flex-column">
      <NavLink to={match.path + routes.mainTable} exact activeClassName="active">
        <i className="fas fa-table"></i>
        Main Table
      </NavLink>
      <NavLink to={match.path + routes.personalInfo} activeClassName="active">
        <i className="fas fa-user"></i>
        Account Details
      </NavLink>
      <NavLink to={match.path + routes.smthElse} activeClassName="active">
        <i className="fas fa-birthday-cake"></i>
        Something else
      </NavLink>
    </aside>
  );
};

export default Aside;
