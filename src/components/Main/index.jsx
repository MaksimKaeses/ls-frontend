import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import routes from '../../config/routes';

import Aside from './Aside';
import Table from './Table';
import SmthElse from './SmthElse';
import PersonalInfo from './PersonalInfo';

const Home = ({ match }) => {
  return (
    <Fragment>
      <main className="flex">
        <Aside match={match} />
        <Fragment>
          <Switch>
            <Route path={match.path + routes.mainTable} render={() => <Table />} />
            <Route path={match.path + routes.smthElse} render={() => <SmthElse />} />
            <Route path={match.path + routes.personalInfo} render={() => <PersonalInfo />} />
          </Switch>
        </Fragment>
      </main>
    </Fragment>
  );
};

export default Home;
