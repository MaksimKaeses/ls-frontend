import React from 'react';

const TableItem = ({ id, firstName, secondName, comment, handleDelete }) => {
  return (
    <tr>
      <td>{id}</td>
      <td>{firstName}</td>
      <td>{secondName}</td>
      <td>{comment}</td>
      <td className="delete-item">
        <i className="fas fa-trash-alt" onClick={() => handleDelete(id)}></i>
      </td>
    </tr>
  );
};

export default TableItem;
