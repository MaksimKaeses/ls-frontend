import notification from "../../../utils/notification";
import api from "../../../config/api";

export const getTableData = () => {
  return async dispatch => {
    dispatch({ type: 'GET_TABLE_DATA_LOADING' });
    try {
      const { data, status } = await api.getTableData();
      if (status > 199 && status < 300) {
        dispatch({ type: 'GET_TABLE_DATA_SUCCESS', data });
      }
    } catch (error) {
      notification('error', error.response.data.error)
    }
  }
}

export const insertNewData = (item) => {
  return async dispatch => {
    dispatch({ type: 'INSERT_NEW_DATA_LOADING' })
    try {
      const { data, status } = await api.insertNewData(item)
      if (status > 199 && status < 300) {
        dispatch({ type: 'INSERT_NEW_DATA_SUCCESS', data })
        notification('info', 'New data has been successfully created')
      }
    } catch (error) {
      console.error(error)
      dispatch({ type: 'INSERT_NEW_DATA_FAILURE' })
      notification('error', error.response.data.error)
    }
  }
}

export const deleteData = id => {
  return async dispatch => {
    console.log(36, { id })
    try {
      const { status } = await api.deleteData(id);
      if (status > 199 && status < 300) {
        dispatch({ type: 'DELETE_DATA_SUCCESS', id })
        notification('info', 'Data has been deleted')
      }
    } catch (error) {
      console.error(error)
    }
  }
}
