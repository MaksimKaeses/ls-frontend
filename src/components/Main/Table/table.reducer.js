const initialState = { data: [], loading: false }

export const tableReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_TABLE_DATA_LOADING':
      return { ...state, loading: true }
    case 'GET_TABLE_DATA_SUCCESS':
      return { ...state, data: action.data.data, loading: false }
    case 'INSERT_NEW_DATA_SUCCESS':
      let data = state.data;
      data.push(action.data)
      return { ...state, data: [...data] }
    case 'DELETE_DATA_SUCCESS':
      const newArray = state.data.filter(item => item.id !== action.id)
      return { ...state, data: newArray }
    case 'LOG_OUT':
      return initialState
    default:
      return state;
  }
};