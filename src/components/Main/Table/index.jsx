import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTableData, deleteData } from './table.action';
import TableItem from './TableItem';
import AddDataModal from './Modal';

const Table = () => {
  const [modalIsOpen, toogleModal] = useState(false);
  const dispatch = useDispatch();
  const data = useSelector(state => state.table.data);

  const handleDelete = id => {
    dispatch(deleteData(id));
  };

  useEffect(() => {
    dispatch(getTableData());
  }, [dispatch]);

  return (
    <section className="data-container flex-column">
      <div className="table-buttons flex">
        <button onClick={() => toogleModal(true)}>Add +</button>
      </div>
      {modalIsOpen && <AddDataModal toogle={toogleModal} />}
      <div className="table-container flex">
        <table>
          <tbody>
            <tr>
              <th>Id</th>
              <th>First Name</th>
              <th>Second Name</th>
              <th>Comment</th>
              <th></th>
            </tr>
            {data && data.length > 0 ? (
              data.map(item => <TableItem key={item.id} {...item} handleDelete={handleDelete} />)
            ) : (
              <tr>
                <td colSpan="5" className="empty-item">
                  Nothing to show
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default Table;
