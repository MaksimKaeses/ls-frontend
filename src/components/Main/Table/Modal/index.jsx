import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { insertNewData } from '../table.action';

const AddDataModal = ({ toogle }) => {
  const [firstName, setFirstName] = useState('');
  const [secondName, setSecondName] = useState('');
  const [comment, setComment] = useState('');

  const ref = useRef();
  const dispatch = useDispatch();

  const handleClickOutside = e => {
    if (ref.current && !ref.current.contains(e.target)) {
      toogle(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside);
    return () => {
      document.removeEventListener('click', handleClickOutside);
    };
  });

  const handleSubmitData = () => {
    dispatch(insertNewData({ firstName, secondName, comment }));
    toogle(false);
  };

  return (
    <section className="modal">
      <div className="wrapper">
        <div className="modal-container flex-column" ref={ref}>
          <div className="modal-head flex-column">
            <button onClick={() => toogle(false)} id="modal-close">
              close
            </button>
            <h1>Add new data</h1>
          </div>
          <form className="flex-column">
            <input
              type="text"
              placeholder="First name"
              value={firstName}
              onChange={e => setFirstName(e.target.value)}
            />
            <input
              type="text"
              placeholder="Second name"
              value={secondName}
              onChange={e => setSecondName(e.target.value)}
            />
            <textarea
              cols="30"
              rows="10"
              placeholder="Comment"
              value={comment}
              onChange={e => setComment(e.target.value)}
            />
          </form>
          <button className="modal-submit" onClick={() => handleSubmitData()}>
            Add
          </button>
        </div>
      </div>
    </section>
  );
};

export default AddDataModal;
