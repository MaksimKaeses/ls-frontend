import React from 'react';
import { useSelector } from 'react-redux';

const PersonalInfo = () => {
  const user = useSelector(state => state.auth.user);
  console.log(window.location);
  return (
    <div className="data-container flex-column">
      <div className="personal-info flex">
        <p>Email: </p>
        <span>{user.email}</span>
      </div>
      <div className="personal-info flex">
        <p>API Key: </p>
        <span>{user.apiKey}</span>
      </div>
    </div>
  );
};

export default PersonalInfo;
