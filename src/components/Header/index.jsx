import React, { Fragment } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { signOut } from '../Auth/auth.action';

const Header = ({ auth }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleLogOut = () => {
    dispatch(signOut());
    history.push('/');
  };

  return (
    <header className="flex">
      <div className="logo flex">
        <h1>Logo</h1>
        <nav className="flex">
          <NavLink to="/" exact activeClassName="active">
            Home
          </NavLink>
          {auth ? (
            <NavLink to="/data" activeClassName="active">
              Main
            </NavLink>
          ) : (
            <Fragment>
              <NavLink to="/sign-in">Sign In</NavLink>
              <NavLink to="/sign-up">Sign Up</NavLink>
            </Fragment>
          )}
        </nav>
      </div>
      {auth && (
        <div>
          <button id="logout" onClick={() => handleLogOut()}>
            <i className="fas fa-sign-out-alt"></i>
            Logout
          </button>
        </div>
      )}
    </header>
  );
};

export default Header;
