import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { signUp } from './auth.action';
import notification from '../../utils/notification';

const SignUp = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const apiKey = useSelector(state => state.auth.user.apiKey);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password1, setPassword1] = useState('');

  useEffect(() => {
    if (apiKey) history.push('/');
  }, [apiKey, history]);

  const handleSubmit = () => {
    if (password !== password1) return notification('error', 'Passwords do not match');
    dispatch(signUp({ email, password }, history));
  };

  return (
    <div className="wrapper">
      <div className="auth-container flex-column">
        <h1>Crate New Account</h1>
        <form className="auth flex-column" action="">
          <input type="email" id="email" placeholder="Email" onChange={e => setEmail(e.target.value)} value={email} />
          <input
            type="password"
            id="password"
            placeholder="Password"
            onChange={e => setPassword(e.target.value)}
            value={password}
          />
          <input
            type="password"
            id="password1"
            placeholder="Repeat password"
            onChange={e => setPassword1(e.target.value)}
            value={password1}
          />
        </form>
        <input type="submit" value="Sign Up" onClick={() => handleSubmit()} />
      </div>
    </div>
  );
};

export default SignUp;
