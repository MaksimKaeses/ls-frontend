import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { signIn } from './auth.action';

const SignIn = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const apiKey = useSelector(state => state.auth.user.apiKey);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    if (apiKey) history.push('/');
  }, [apiKey, history]);

  const handleEmailChange = e => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = e => {
    setPassword(e.target.value);
  };

  const handleSubmit = () => {
    dispatch(signIn({ email, password }));
  };

  return (
    <div className="wrapper">
      <div className="auth-container flex-column">
        <h1>Welcome back!</h1>
        <form className="auth flex-column" action="">
          <input type="email" name="email" id="email" placeholder="Email" onChange={handleEmailChange} value={email} />
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Password"
            onChange={handlePasswordChange}
            value={password}
          />
        </form>
        <input type="submit" value="Sign In" onClick={() => handleSubmit()} />
      </div>
    </div>
  );
};

export default SignIn;
