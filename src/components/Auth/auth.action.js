import api from "../../config/api";
import routes from '../../config/routes'
import notifiaction from '../../utils/notification'
import setAuthToken from '../../utils/setAuthToken'

export const signUp = (credentials, history) => {
  return async dispatch => {
    dispatch({ type: 'SIGN_UP_LOADING' });

    try {
      const { data, status } = await api.signUp(credentials);
      if (status > 199 && status < 300 && data.message === 'Success') {
        notifiaction('info', 'New account has been successfully created. Please log in now');
        history.push(routes.signIn)
      }
    } catch (error) {
      notifiaction('error', error.response.data.error)
    }
  }
}

export const signIn = credentials => {
  return async dispatch => {
    dispatch({ type: 'SIGN_IN_LOADING' });

    try {
      const { data, status } = await api.signIn(credentials);
      if (status > 199 && status < 300) {
        dispatch({ type: 'SIGN_IN_SUCCESS', data });
        notifiaction('info', `You've logged in successfully`);
        window.localStorage.setItem('LS-FRONTEND', data.token);
        setAuthToken(data.token);
      } else {
        dispatch({ type: 'SIGN_IN_FAILURE' })
      }
    } catch (error) {
      notifiaction('error', error.response.data.error)
      dispatch({ type: 'SIGN_IN_FAILURE' })
    }
  };
};

export const getUserData = () => {
  return async dispatch => {
    dispatch({ type: 'GET_USER_LOADING' });
    try {
      const { data, status } = await api.getUser();
      if (status > 199 && status < 300) {
        dispatch({ type: 'GET_USER_SUCCESS', data })
      }
    } catch (error) {
      console.error(error.response)
      notifiaction('error', error.response.data.error)
    }
  }
}

export const signOut = () => {
  return async dispatch => {
    window.localStorage.removeItem('LS-FRONTEND')
    dispatch({ type: 'LOG_OUT' })
  }
}