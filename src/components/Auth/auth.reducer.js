const initialState = { user: {}, loading: false, error: false }

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SIGN_IN_LOADING":
      return { ...state, loading: true, error: false };
    case "SIGN_IN_SUCCESS":
      return { ...state, user: action.data, loading: false, error: false }
    case "SIGN_IN_FAILURE":
      return { ...state, loading: false, error: true }
    case "GET_USER_LOADING":
      return { ...state, loading: true }
    case "GET_USER_SUCCESS":
      return { ...state, user: action.data, loading: false, error: false }
    case 'LOG_OUT':
      return initialState
    default:
      return state;
  }
};